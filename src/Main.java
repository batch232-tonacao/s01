public class Main {
//    Main Class
//    entry point of our java program
//    main class has 1 method inside, the main method => to run our code
    public static void main(String[] args) {
        /*
            "public" - access modifier which simple tells the application which classes have access to our method/attributes
            "static" - method or property that belongs to the class. It is accessible without having to create and instance of an object
            "void" - the method will not return any data. Because in Java we have to declare the data type of the method's return
        */
//        System.out.println is a statement that allows us to print the value off the arguments passed into its terminal
        System.out.println("Hello world!");
        System.out.println("Hello Again!");
    }
}