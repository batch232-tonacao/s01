package com.zuitt.example;

public class Variables {
    public static void main(String[] args){
//        variable
        int age;
        char middle_name;

//        variable initialization
        int x = 0;
        char letter = 'a';

//        boolean
        boolean isMVP = true;
        boolean isChampion = false;
        System.out.println(isMVP);
        System.out.println(isChampion);

//        constants in Java
//        final dataType VARIABLENAME
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

//        String - non primitive data type. String are actually object that can use methods
        String username = "theTinker23";
        System.out.println(username.isEmpty());//returns false
    }
}
