package com.zuitt.example;

import java.util.Scanner;

public class Activity {
    public static void main(String[] args){
        Scanner myInput = new Scanner(System.in);
        System.out.println("Enter First Name:");
        String firstName = myInput.nextLine();
        System.out.println("Enter Last Name:");
        String lastName = myInput.nextLine();
        System.out.println("Enter First Subject Grade:");
        double grade1 = Double.parseDouble(myInput.nextLine());
        System.out.println("Enter Second Subject Grade:");
        double grade2 = Double.parseDouble(myInput.nextLine());
        System.out.println("Enter Third Subject Grade:");
        double grade3 = Double.parseDouble(myInput.nextLine());
        double ave = (grade1+grade2+grade3) / 3;
        System.out.println("Good Day, "+firstName+" "+lastName);
        System.out.println("Your Average grade is "+ave);

    }
}
